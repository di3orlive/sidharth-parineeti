jQuery.exists = function (selector) {
    return ($(selector).length > 0);
};


(function() {
    $(document).ready(function() {

        $('.menu').on('click', function(){
            $('.hide-menu').slideToggle('fast');
            $('body').toggleClass('active');
            $('.menu').toggleClass('active');
        });

    });
}).call(this);